import matplotlib.pyplot as plt
import matplotlib.colors as col
import numpy as np
from yaml import load

# load config from yaml file
try:
    stream = open('skills.yaml', 'rb')
except FileNotFoundError:
    print("[ERROR] No config file found. Looking for skills.yaml. You can create a skills.yaml based on the provided skills_dist.yaml")
    exit()
    
config = load(stream)

# extract data from config
skill_groups = config["Skills"]

# skill_groups are displayed blocks on the x-axes
nr_skill_groups = len(skill_groups)
nr_skills = sum(len(group_skills.values())for _, group_skills in skill_groups.items())

# times, projects or anything else you used your skills at
# todo: put into external config file
y_labels = ["Privat", "Bachelor", "Bachelorthesis", "Master", "Masterthesis"]
y_label_value_map = {}
y_tick = 1
y_ticks = []
for y_label in y_labels:
    # map label string to y value, enum style
    y_label_value_map[y_label] = y_tick
    # append to tick list
    y_ticks.append(y_tick)
    y_tick += 1

# levels to describe how much you used a skill
# todo: put into external config file
usage = ["wenig", "mittel", "viel"]
usage_value_map = {}
value = 1
for intensity in usage:
    # map usage intensity string to value, again enum style
    usage_value_map[intensity] = value
    value += 1

# create matrix for plotting data
# y dimension of the plot, use highest y_tick
y_dim = y_ticks[-1] + 1
# x dimension of the plot, increase x_dim to leave gaps between skill_groups
# -1 because fencepost error
x_dim =  nr_skills + nr_skill_groups - 1 +1
skill_matrix = np.zeros((y_dim, x_dim)) 

# iterate through all skills and collect plot data 
# x-axes. skip one tik between skill_groups
x_labels = []
x_ticks = []
x_tick = 1
for group_name, skills in skill_groups.items():
    for skill, usage in skills.items():
        for y_label in y_labels:
            # intensity of the skill at y_label
            usage_string = usage.get(y_label)
            try:
                cell_value = usage_value_map[usage_string]
            except KeyError:
                cell_value = 0
                
            # could also use enumerate(y_labels) for ticks but maybe
            # it comes in handy to shuffle those y_ticks around
            y_tick = y_label_value_map[y_label]
            skill_matrix[y_tick, x_tick] = cell_value
            
        x_labels.append(skill)
        x_ticks.append(x_tick)
        x_tick+=1
        
    x_tick+=1

# todo: put into external config file
max_saturation = config.get("color_max_saturation", 0.8)
h = config.get("color_hue", 0.6)
v = 1.0

# create discrete/qualitative color map: one color, increasing satuation 
s_values = np.linspace(0, max_saturation, len(usage)+2)
colors = []
for s in s_values:
    colors.append(col.hsv_to_rgb([h, s, v]))
cmap = col.ListedColormap(colors)
    

# plot that shit
fig = plt.figure()
ax = plt.gca()
ax.matshow(skill_matrix, cmap=cmap)
plt.xticks(x_ticks, x_labels)
plt.yticks(y_ticks, y_labels)
plt.xlim(0, x_dim)
plt.ylim(0, y_dim)

# Move x ticks to the bottom of the plot and rotate them
ax.xaxis.set_ticks_position("bottom")
plt.setp( ax.xaxis.get_majorticklabels(), rotation=45 , ha="right") 


# Minor ticks with 0.5 offset to create a border of every block
ax.set_xticks(np.arange(0.5, x_dim, 1), minor=True);
ax.set_yticks(np.arange(0.5, y_dim, 1), minor=True);

# Gridlines based on minor ticks
ax.grid(which='minor', color='w', linestyle='-', linewidth=2)
# Hide the small lines for the minor ticks along the axes
for tic in ax.xaxis.get_minor_ticks():
    tic.tick1On = tic.tick2On = False
for tic in ax.yaxis.get_minor_ticks():
    tic.tick1On = tic.tick2On = False
    
plt.draw()
plt.show()
plt.savefig("SkillPlot.png")
